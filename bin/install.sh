#!/bin/bash
# Deploy Application To Development


echo
echo "Stopping server..."
echo
service nginx stop

cd /usr/share/nginx
rm -rf event-manager-wui.local

read -p "Select name of the branch: " BRANCH_NAME
echo

echo "Downloading latest source code from branch: $BRANCH_NAME"
echo

git clone https://bba-bitbucket.enjoying.rs/scm/inco/event-manager-wui.git -b "$BRANCH_NAME" event-manager-wui.local

if [ $? -ne 0 ]
then
	echo "Downloading application failed. Error code: $?"
	exit $?
fi;

echo
echo "Compiling application. Configuring for: $1"
echo

cd event-manager-wui.local
echo
declare -r ENV_FILE_NAME=".${1}_ENV"
cp -r $ENV_FILE_NAME ".env"

echo
npm install
npm run build

if [ $? -ne 0 ]
then
	echo "Compiling application failed. Error code: $?"
	exit $?
fi;

echo "Linking production code with server..."
echo 

rm /usr/share/nginx/current
ln -s /usr/share/nginx/event-manager-wui.local/release-build /usr/share/nginx/current

echo "Starting server..."
echo
service nginx start
