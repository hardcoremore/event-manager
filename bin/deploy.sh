#!/bin/bash
# Deploy Application To Development


echo 

read -p "Pleaase enter environment where to deploy application. Valid options are DEV, STAG, PROD: " ENV

echo

declare -r SERVER_ADDRESS_DEV="root@167.99.244.1"
declare -r SERVER_ADDRESS_STAG="root@167.99.244.1"
declare -r SERVER_ADDRESS_PROD="root@167.99.244.1"

if [ $ENV == "DEV" -o $ENV == "STAG" -o $ENV == "PROD" ]
then
	echo "Deploying to $ENV:"
	echo
	
	echo
	echo "Connecting to server..."
	echo

	declare -r SERVER_ADDRESS="SERVER_ADDRESS_$ENV"
	declare -r SCRIPT_PATH=${BASH_SOURCE%/*}/install.sh;

	ssh -tt ${!SERVER_ADDRESS} "set -- $ENV;$(cat $SCRIPT_PATH)"

else
	echo "You have entered invalid environment: $ENV"
fi;


