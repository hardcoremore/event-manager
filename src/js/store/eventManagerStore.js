import { createStore, compose, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootSagaStarter from './sagas';
import { getActiveUserData } from '../services/authService';

// if directory name is provided node will resolve to be index.js if reducers file does not exists
import reducer from './reducers';

let eventManagerStore;

export default () => eventManagerStore;

export const initializeStore = () => {
  const sagaMiddleware = createSagaMiddleware();

  eventManagerStore = createStore(
    reducer,
    {
      authentication: {
        activeUser: getActiveUserData()
      }
    },
    compose(
      applyMiddleware(sagaMiddleware),
      typeof window === 'object' && typeof window.devToolsExtension !== 'undefined'
        ? window.devToolsExtension()
        : f => f
    )
  );

  rootSagaStarter(sagaMiddleware);
};
