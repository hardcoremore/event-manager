import serverResponseErrorCreator from "./serverResponseErrorCreator";
import {errorMessages} from "../helpers/errorMessages"

export default error => {
  let formattedError;

  if (error.response && error.response.data) {
    formattedError = serverResponseErrorCreator(error.response);
  } else {
    formattedError = {
      message: errorMessages.unknownServerError,
      code: error.response.status
    };
  }

  return formattedError;
};
