import BaseApi from './BaseApi';
import axios from 'axios';
import { getAuthTokenData } from '../../services/authService';

export default class AuthenticationApi extends BaseApi {
  async login(username, password) {
    return await axios.post(this.getFullUrl('login'), {
      username,
      password
    });
  }

  async logout() {
    return await axios.post(this.getFullUrl('logout'), null, {
      headers: {
        Authorization: 'Bearer ' + getAuthTokenData().accessToken
      }
    });
  }
}
