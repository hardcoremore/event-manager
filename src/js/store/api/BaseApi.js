import axios from 'axios';

export default class BaseApi {
  constructor(slugPrefix = '') {
    this._baseUrl = process.env.BASE_API_URL;
    this._slugPrefix = slugPrefix;
  }

  get authorizationTokenData() {
    return this._authorizationTokenData;
  }

  set authorizationTokenData(tokenData) {
    this._authorizationTokenData = tokenData;
  }

  get baseUrl() {
    return this._baseUrl;
  }

  set baseUrl(url) {
    this._baseUrl = url;
  }

  get slugPrefix() {
    return this._slugPrefix;
  }

  set slugPrefix(prefix) {
    this._slugPrefix = prefix;
  }

  getFullUrl(prefix = '') {
    let url = this.baseUrl;

    if (this.slugPrefix && this.slugPrefix.length > 0) {
      url += '/' + this.slugPrefix;
    }

    if (prefix && prefix.toString().length > 0) {
      url += '/' + prefix;
    }

    return url;
  }

  async read(queryParameters = null) {
    return await axios.get(this.getFullUrl(), {
      params: queryParameters,
      headers: {
        Authorization: 'Bearer ' + this.authorizationTokenData.accessToken
      }
    });
  }

  async create(postParameters) {
    return await axios.post(this.getFullUrl(), postParameters, {
      headers: {
        Authorization: 'Bearer ' + this.authorizationTokenData.accessToken
      }
    });
  }

  async readById(id) {
    return await axios.get(this.getFullUrl(id), {
      headers: {
        Authorization: 'Bearer ' + this.authorizationTokenData.accessToken
      }
    });
  }

  async update(id, data) {
    return await axios.put(this.getFullUrl(id), data, {
      headers: {
        Authorization: 'Bearer ' + this.authorizationTokenData.accessToken
      }
    });
  }

  async delete(id) {
    return await axios.delete(this.getFullUrl(id), {
      headers: {
        Authorization: 'Bearer ' + this.authorizationTokenData.accessToken
      }
    });
  }
}
