import BaseApi from './BaseApi';
import axios from 'axios';

export default class AuthenticationApi extends BaseApi {
  async readAttendSeminars(queryParameters = null) {
    return await axios.get(this.getFullUrl('attend'), {
      params: queryParameters,
      headers: {
        Authorization: 'Bearer ' + this.authorizationTokenData.accessToken
      }
    });
  }

  async create(companyId, seminarData) {
    return await axios.post(this.getFullUrl(companyId + '/company'), seminarData, {
      headers: {
        Authorization: 'Bearer ' + this.authorizationTokenData.accessToken
      }
    });
  }
}
