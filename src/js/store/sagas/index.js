import "regenerator-runtime/runtime"; //sagas are dependent on this regenerator run time which is babel dependency
import authentication from "./authentication";
import company from "./company";
import seminar from "./seminar";

export default function initAllSagas(sagaMiddleware) {
  sagaMiddleware.run(authentication);
  sagaMiddleware.run(company);
  sagaMiddleware.run(seminar);
}
