import { put, takeEvery, all, apply } from 'redux-saga/effects';
import actionCreator from '../actionCreator';
import seminarActions from '../actions/seminar';
import ApiFactory from '../../factory/ApiFactory';
import sagaErrorCreator from '../sagaErrorCreator';

let seminarApi;

function* readSeminars() {
  try {
    const readResponse = yield apply(seminarApi, 'read');
    yield put(actionCreator(seminarActions.setSeminars, readResponse.data.data));
  } catch (error) {
    yield put(actionCreator(seminarActions.readRequestError, sagaErrorCreator(error)));
  }
}

function* readSeminarById(action) {
  try {
    const readResponse = yield apply(seminarApi, 'readById', [action.payload]);
    yield put(actionCreator(seminarActions.setSeminarById, readResponse.data.data));
  } catch (error) {
    yield put(actionCreator(seminarActions.readSeminarByRequestError, sagaErrorCreator(error)));
  }
}

function* readAttendSeminars() {
  try {
    const readResponse = yield apply(seminarApi, 'readAttendSeminars');
    yield put(actionCreator(seminarActions.setAttendSeminars, readResponse.data.data));
  } catch (error) {
    yield put(
      actionCreator(seminarActions.readAttendSeminarsRequestError, sagaErrorCreator(error))
    );
  }
}

function* readEditSeminars() {
  try {
    const readResponse = yield apply(seminarApi, 'read');
    yield put(actionCreator(seminarActions.setEditSeminars, readResponse.data.data));
  } catch (error) {
    yield put(actionCreator(seminarActions.readEditSeminarsRequestError, sagaErrorCreator(error)));
  }
}

function* createSeminar(action) {
  try {
    const createResponse = yield apply(seminarApi, 'create', [
      action.payload.companyId,
      action.payload.data
    ]);

    yield put(actionCreator(seminarActions.addSeminar, createResponse.data.data));
    yield put(actionCreator(seminarActions.setLastCreatedSeminar, createResponse.data.data));
  } catch (error) {
    yield put(actionCreator(seminarActions.createRequestError, sagaErrorCreator(error)));
  }
}

function* updateSeminar(action) {
  try {
    const updateResponse = yield apply(seminarApi, 'update', [
      action.payload.recordId,
      action.payload.data
    ]);
    yield put(
      actionCreator(seminarActions.updateSeminar, {
        id: action.payload.recordId,
        payload: updateResponse.data.data
      })
    );

    yield put(actionCreator(seminarActions.setLastUpdatedSeminar, updateResponse.data.data));
  } catch (error) {
    yield put(actionCreator(seminarActions.updateRequestError, sagaErrorCreator(error)));
  }
}

function* deleteSeminar(action) {
  try {
    yield apply(seminarApi, 'delete', [action.payload]);
    yield put(actionCreator(seminarActions.removeSeminar, action.payload));
  } catch (error) {
    yield put(actionCreator(seminarActions.deleteRequestError, sagaErrorCreator(error)));
  }
}

function* watchSeminarRequest() {
  yield takeEvery(seminarActions.sendReadSeminarByIdRequest, readSeminarById);

  yield takeEvery(seminarActions.sendReadSeminarsRequest, readSeminars);

  yield takeEvery(seminarActions.sendAttendSeminarReadRequest, readAttendSeminars);
  yield takeEvery(seminarActions.sendEditSeminarReadRequest, readEditSeminars);

  yield takeEvery(seminarActions.sendCreateRequest, createSeminar);
  yield takeEvery(seminarActions.sendUpdateRequest, updateSeminar);
  yield takeEvery(seminarActions.sendDeleteRequest, deleteSeminar);
}

// notice how we now only export the rootSaga
// single entry point to start all Sagas at once
export default function* rootSaga() {
  seminarApi = ApiFactory.createSeminarApi();
  yield all([watchSeminarRequest()]);
}
