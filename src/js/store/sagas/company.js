import { put, takeEvery, all, apply } from 'redux-saga/effects';
import actionCreator from '../actionCreator';
import companyActions from '../actions/company';
import ApiFactory from '../../factory/ApiFactory';
import sagaErrorCreator from '../sagaErrorCreator';

// saga tutorial
// http://thecodebarbarian.com/redux-saga-vs-async-await

let companyApi;

function* readCompanies() {
  try {
    const readResponse = yield apply(companyApi, 'read');
    yield put(actionCreator(companyActions.setCompanies, readResponse.data.data));
  } catch (error) {
    yield put(actionCreator(companyActions.readRequestError, sagaErrorCreator(error)));
  }
}

function* createCompany(action) {
  try {
    const createResponse = yield apply(companyApi, 'create', [action.payload]);
    yield put(actionCreator(companyActions.addCompany, createResponse.data.data));
    yield put(actionCreator(companyActions.setLastCreatedCompany, createResponse.data.data));
  } catch (error) {
    yield put(actionCreator(companyActions.createRequestError, sagaErrorCreator(error)));
  }
}

function* updateCompany(action) {
  try {
    const updateResponse = yield apply(companyApi, 'update', [
      action.payload.recordId,
      action.payload.data
    ]);
    yield put(
      actionCreator(companyActions.updateCompany, {
        id: action.payload.recordId,
        payload: updateResponse.data.data
      })
    );

    yield put(actionCreator(companyActions.setLastUpdatedCompany, updateResponse.data.data));
  } catch (error) {
    yield put(actionCreator(companyActions.updateRequestError, sagaErrorCreator(error)));
  }
}

function* deleteCompany(action) {
  try {
    yield apply(companyApi, 'delete', [action.payload]);
    yield put(actionCreator(companyActions.removeCompany, action.payload));
  } catch (error) {
    yield put(actionCreator(companyActions.deleteRequestError, sagaErrorCreator(error)));
  }
}

function* watchCompanyRequests() {
  yield takeEvery(companyActions.sendReadRequest, readCompanies);
  yield takeEvery(companyActions.sendCreateRequest, createCompany);
  yield takeEvery(companyActions.sendUpdateRequest, updateCompany);
  yield takeEvery(companyActions.sendDeleteRequest, deleteCompany);
}

// notice how we now only export the rootSaga
// single entry point to start all Sagas at once
export default function* rootSaga() {
  companyApi = ApiFactory.createCompanyApi();
  yield all([watchCompanyRequests()]);
}
