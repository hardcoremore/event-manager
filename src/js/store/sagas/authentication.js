import { put, all, apply, takeLatest } from 'redux-saga/effects';
import actionCreator from '../actionCreator';
import authActions from '../actions/authentication';
import ApiFactory from '../../factory/ApiFactory';
import sagaErrorCreator from '../sagaErrorCreator';
import { completeLogin, completeLogout } from '../../services/authService';

// saga tutorial
// http://thecodebarbarian.com/redux-saga-vs-async-await

const authApi = ApiFactory.createAuthenticationApi();

function* loginUser(action) {
  try {
    const authResponse = yield apply(authApi, 'login', [
      action.payload.username,
      action.payload.password
    ]);
    yield put(actionCreator(authActions.setActiveUser, authResponse.data.user));
    completeLogin(authResponse.data);
  } catch (error) {
    yield put(actionCreator(authActions.loginRequestError, sagaErrorCreator(error)));
  }
}

function* logoutUser() {
  try {
    yield apply(authApi, 'logout');
    yield put(actionCreator(authActions.setActiveUser, null));
    completeLogout();
  } catch (error) {
    yield put(actionCreator(authActions.loginRequestError, sagaErrorCreator(error)));
  }
}

function* watchLoginRequest() {
  //  takeLatest function will ensure that `saga` runs every time a `FETCH_USER` action comes
  // in, but only one `FETCH_USER` action can run at a time
  yield takeLatest(authActions.sendLoginRequest, loginUser);
  yield takeLatest(authActions.sendLogoutRequest, logoutUser);
}

// notice how we now only export the rootSaga
// single entry point to start all Sagas at once
export default function* rootSaga() {
  yield all([watchLoginRequest()]);
}
