import { errorMessages } from '../helpers/errorMessages';

export default response => {
  const error = {
    code: response.status
  };

  switch (response.status) {
    case 422:
      error.message = errorMessages.formIsNotValid;
      error.errors = response.data.errors;
      break;

    case 500:
      error.message = errorMessages.unknownServerError;
      break;
  }

  return error;
};
