import companyActions from '../actions/company';

const initialState = {
  recordList: [],
  lastCreatedCompany: null,
  lastUpdatedCompany: null,
  readError: null,
  createError: null,
  updateError: null
};

let record, recordIndex, newRecordList;

export default function companyReducer(state = initialState, action) {
  switch (action.type) {
    case companyActions.readRequestError:
      return { ...state, readError: action.payload, recordList: [] };
      break;

    case companyActions.setCompanies:
      return { ...state, recordList: action.payload };
      break;

    case companyActions.addCompany:
      state.recordList.push(action.payload);
      return { ...state, createError: null };
      break;

    case companyActions.sendCreateRequest:
      return { ...state, createError: null, lastCreatedCompany: null };
      break;

    case companyActions.sendUpdateRequest:
      return { ...state, updateError: null, lastUpdatedCompany: null };
      break;

    case companyActions.setLastCreatedCompany:
      return { ...state, lastCreatedCompany: action.payload, createError: null };
      break;

    case companyActions.setLastUpdatedCompany:
      return { ...state, lastUpdatedCompany: action.payload, updateError: null };
      break;

    case companyActions.updateCompany:
      record = state.recordList.find(item => item.id === action.payload.id);

      newRecordList = state.recordList.slice(0);
      recordIndex = newRecordList.indexOf(record);
      newRecordList[recordIndex] = action.payload;

      return { ...state, recordList: newRecordList, updateError: null };
      break;

    case companyActions.createRequestError:
      return { ...state, createError: action.payload };
      break;

    case companyActions.updateRequestError:
      return { ...state, updateError: action.payload };
      break;

    case companyActions.removeCompany:
      return {
        ...state,
        recordList: state.recordList.filter(
          item => item.id.toString() !== action.payload.toString()
        ),
        deleteError: null
      };
      break;

    default:
      return state;
      break;
  }
}
