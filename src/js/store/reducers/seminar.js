import seminarActions from '../actions/seminar';

const initialState = {
  recordList: [],
  seminarById: null,
  attendRecordList: [],
  editRecordList: [],
  lastCreatedSeminar: null,
  lastUpdatedSeminar: null,
  readError: null,
  createError: null,
  updateError: null
};

let record, recordIndex, newRecordList, newEditRecordList;

export default function seminarReducer(state = initialState, action) {
  switch (action.type) {
    case seminarActions.sendReadSeminarsRequest:
      return { ...state, readError: null, recordList: [] };
      break;

    case seminarActions.setSeminars:
      return { ...state, recordList: action.payload };
      break;

    case seminarActions.setSeminarById:
      return { ...state, seminarById: action.payload };
      break;

    case seminarActions.readAttendSeminarsRequestError:
      return { ...state, serverError: action.payload, attendRecordList: [] };
      break;

    case seminarActions.setAttendSeminars:
      return { ...state, attendRecordList: action.payload };
      break;

    case seminarActions.readEditSeminarsRequestError:
      return { ...state, serverError: action.payload, editRecordList: [] };
      break;

    case seminarActions.setEditSeminars:
      return { ...state, editRecordList: action.payload };
      break;

    case seminarActions.createRequestError:
      return { ...state, createError: action.payload };
      break;

    case seminarActions.updateRequestError:
      return { ...state, updateError: action.payload };
      break;

    case seminarActions.addSeminar:
      state.recordList.push(action.payload);

      return {
        ...state,
        recordList: state.recordList.slice(0).push(action.payload),
        editRecordList: state.editRecordList.filter(seminarData => {
          if (seminarData.id === action.payload.company.id) {
            seminarData.seminars = seminarData.seminars.slice(0);
            seminarData.seminars.push(action.payload);
          }
          return true;
        }),
        createError: null
      };
      break;

    case seminarActions.updateSeminar:
      record = state.recordList.find(item => item.id === action.payload.id);

      if (record) {
        newRecordList = state.recordList.slice(0);
        recordIndex = newRecordList.indexOf(record);
        newRecordList[recordIndex] = action.payload;
      } else {
        newRecordList = state.recordList;
      }

      newEditRecordList = state.editRecordList.filter(seminarData => {
        if (seminarData.id === action.payload.company_id) {
          seminarData.seminars = seminarData.seminars.slice(0);

          recordIndex = seminarData.seminars.indexOf(record);
          seminarData.seminars[recordIndex] = action.payload;
        }

        return true;
      });

      return {
        ...state,
        recordList: newRecordList,
        editRecordList: newEditRecordList,
        updateError: null
      };

      break;

    case seminarActions.sendCreateRequest:
      return { ...state, createError: null, lastCreatedSeminar: null };
      break;

    case seminarActions.sendUpdateRequest:
      return { ...state, updateError: null, lastUpdatedSeminar: null };
      break;

    case seminarActions.setLastCreatedSeminar:
      return { ...state, lastCreatedSeminar: action.payload, createError: null };
      break;

    case seminarActions.setLastUpdatedSeminar:
      return { ...state, lastUpdatedSeminar: action.payload, updateError: null };
      break;

    case seminarActions.removeSeminar:
      return {
        ...state,
        recordList: state.recordList.filter(
          item => item.id.toString() !== action.payload.toString()
        ),
        attendRecordList: state.attendRecordList.filter(
          item => item.id.toString() !== action.payload.toString()
        ),
        editRecordList: state.editRecordList.filter(item => {
          const seminars = item.seminars.filter(
            seminar => seminar.id.toString() !== action.payload.toString()
          );
          item.seminars = seminars;
          return seminars.length > 0;
        }),
        deleteError: null
      };

      break;

    default:
      return state;
      break;
  }
}
