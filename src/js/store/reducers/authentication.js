import authActions from '../actions/authentication';

const initialState = {};

export default function authenticationReducer(state = initialState, action) {
  switch (action.type) {
    case authActions.loginRequestError:
      return { loginError: action.payload, activeUser: null };
      break;

    case authActions.setActiveUser:
      return { activeUser: action.payload, loginError: null };
      break;

    default:
      return state;
      break;
  }
}
