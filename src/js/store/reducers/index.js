import { combineReducers } from 'redux';
import authentication from './authentication';
import company from './company';
import seminar from './seminar';

export default combineReducers({
  authentication,
  company,
  seminar
});
