export default {

  sendReadSeminarByIdRequest: 'SEND_READ_SEMINAR_BY_ID_REQUEST',
  setSeminarById: 'SET_SEMINAR_BY_ID',
  readSeminarByRequestError: 'READ_SEMINAR_BY_ID_REQUEST_ERROR',

  sendReadSeminarsRequest: 'SEND_READ_SEMINARS_REQUEST',
  setSeminars: 'SET_SEMINARS',
  readSeminarsRequestError: 'READ_SEMINARS_REQUEST_ERROR',

  sendAttendSeminarReadRequest: 'SEND_ATTEND_SEMINARS_READ_REQUEST',
  setAttendSeminars: 'SET_ATTEND_SEMINARS',
  readAttendSeminarsRequestError: 'READ_ATTEND_SEMINARS_REQUEST_ERROR',

  sendEditSeminarReadRequest: 'SEND_EDIT_SEMINARS_READ_REQUEST',
  setEditSeminars: 'SET_EDIT_SEMINARS',
  readEditSeminarsRequestError: 'READ_EDIT_SEMINARS_REQUEST_ERROR',

  setLastCreatedSeminar: 'SET_LAST_CREATED_SEMINARs',
  setLastUpdatedSeminar: 'SET_LAST_UPDATED_SEMINAR',

  sendCreateRequest: 'SEND_CREATE_SEMINAR_REQUEST',
  addSeminar: 'ADD_SEMINAR',
  createRequestError: 'CREATE_SEMINAR_REQUEST_ERROR',

  sendUpdateRequest: 'SEND_UPDATE_SEMINAR_REQUEST',
  updateSeminar: 'UPDATE_SEMINAR',
  updateRequestError: 'UPDATE_SEMINAR_REQUEST_ERROR',

  sendDeleteRequest: 'SEND_DELETE_SEMINAR_REQUEST',
  removeSeminar: 'REMOVE_SEMINAR',
  deleteRequestError: 'DELETE_SEMINAR_REQUEST_ERROR'
};
