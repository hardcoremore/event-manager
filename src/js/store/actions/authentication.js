export default {
  sendLoginRequest: 'SEND_LOGIN_REQUEST',
  loginRequestError: 'LOGIN_REQUEST_ERROR',

  sendLogoutRequest: 'SEND_LOGOUT_REQUEST',

  setActiveUser: 'SET_ACTIVE_USER',

  sendRequestPasswordChangeRequest: 'SEND_REQUEST_PASSWORD_CHANGE_REQUEST',
  setRequestPasswordChangeSuccess: 'SET_REQUEST_PASSWORD_CHANGE_SUCCESS',
  sendRequestPasswordChangeError: 'SET_REQUEST_PASSWORD_CHANGE_ERROR',

  sendPasswordChangeRequest: 'SEND_PASSWORD_CHANGE_REQUEST',
  setPasswordChangeSuccess: 'SET_PASSWORD_CHANGE_SUCCESS',
  setPasswordChangeError: 'SET_PASSWORD_CHANGE_ERROR'
};
