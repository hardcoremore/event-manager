export default {
  sendReadRequest: 'SEND_READ_COMPANIES_REQUEST',
  setCompanies: 'SET_COMPANIES',
  readRequestError: 'READ_COMPANIES_REQUEST_ERROR',

  setLastCreatedCompany: 'SET_LAST_CREATED_COMPANY',
  setLastUpdatedCompany: 'SET_LAST_UPDATED_COMPANY',

  sendCreateRequest: 'SEND_CREATE_COMPANY_REQUEST',
  addCompany: 'ADD_COMPANY',
  createRequestError: 'CREATE_COMPANY_REQUEST_ERROR',

  sendUpdateRequest: 'SEND_UPDATE_COMPANY_REQUEST',
  updateCompany: 'UPDATE_COMPANY',
  updateRequestError: 'UPDATE_COMPANY_REQUEST_ERROR',

  sendDeleteRequest: 'SEND_DELETE_COMPANY_REQUEST',
  removeCompany: 'REMOVE_COMPANY',
  deleteRequestError: 'DELETE_COMPANY_REQUEST_ERROR'
};
