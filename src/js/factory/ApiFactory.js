import BaseApi from '../store/api/BaseApi';
import AuthenticationApi from '../store/api/AuthenticationApi';
import SeminarApi from '../store/api/SeminarApi';
import { getAuthTokenData } from '../services/authService';

let authenticationApi, companyApi, seminarApi;

export default class ApiFactory {
  static createAuthenticationApi() {
    if (!authenticationApi) {
      authenticationApi = new AuthenticationApi('authenticate');
    }

    return authenticationApi;
  }

  static createCompanyApi() {
    if (!companyApi) {
      companyApi = new BaseApi('companies');
      companyApi.authorizationTokenData = getAuthTokenData();
    }

    return companyApi;
  }

  static createSeminarApi() {
    if (!seminarApi) {
      seminarApi = new SeminarApi('seminars');
      seminarApi.authorizationTokenData = getAuthTokenData();
    }

    return seminarApi;
  }
}
