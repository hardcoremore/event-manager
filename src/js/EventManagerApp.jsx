import React from 'react';
import { connect } from 'react-redux';

import { Router } from '@reach/router';

import LoginScreen from './screens/LoginScreen';

import EventManagerScreenTemplate from './screens/templates/EventManagerScreenTemplate';

import HomeScreen from './screens/HomeScreen';
import CompanyOrganiserScreen from './screens/CompanyOrganiserScreen';

import OrganiserScreenTemplate from './screens/templates/OrganiserScreenTemplate';
import SeminarOrganiserScreen from './screens/SeminarOrganiserScreen';
import SeminarConfiguratorScreen from './screens/SeminarConfiguratorScreen';

import SeminarDashboardScreen from './screens/SeminarDashboardScreen';
import SingleSeminarHomeScreen from './screens/SingleSeminarHomeScreen';

class EventManagerApp extends React.Component {
  render() {
    return (
      <Router>
        <LoginScreen path="/login" />
        <EventManagerScreenTemplate path="/">
          <HomeScreen path="/" />

          <OrganiserScreenTemplate path="/organiser">
            <SeminarOrganiserScreen path="/seminar/*" />
            <CompanyOrganiserScreen path="/company/*" />
          </OrganiserScreenTemplate>

          <SeminarConfiguratorScreen path="/organiser/seminar/edit/:recordId/*" />

          <SeminarDashboardScreen path="/seminars" />
          <SingleSeminarHomeScreen path="/seminars/:recordId" />
        </EventManagerScreenTemplate>
      </Router>
    );
  }
}

const mapStateToProps = ({ authentication }) => ({
  authentication
});

export default connect(
  mapStateToProps,
  null
)(EventManagerApp);
