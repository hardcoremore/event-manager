import localForage from "localforage";
import { navigate } from "@reach/router";
import eventManagerStore from "../store/eventManagerStore";

let currentTokenData;
let activeUserData;

export function getAuthTokenData() {
  return currentTokenData;
}

export function getActiveUserData() {
  return activeUserData;
}

export async function loadLoginData() {
  await localForage
    .getItem("authTokenData")
    .then(authData => {
      currentTokenData = authData;
    })
    .catch(error => {
      console.log("Token data load error: ", error);
    });

  await localForage
    .getItem("activeUserData")
    .then(userData => {
      activeUserData = userData;
    })
    .catch(error => {
      console.log("Active user load error: ", error);
    });
}

export function isLoginDataValid() {
  return (
    activeUserData !== undefined &&
    activeUserData !== null &&
    currentTokenData !== undefined &&
    currentTokenData !== null
  );
}

export async function completeLogin(authResponse) {
  currentTokenData = {
    accessToken: authResponse.access_token,
    refreshToken: authResponse.refresh_token,
    expiresIn: authResponse.expires_in
  };

  activeUserData = authResponse.user;

  await localForage.setItem("authTokenData", currentTokenData);
  await localForage.setItem("activeUserData", authResponse.user);

  const userData = eventManagerStore().getState().authentication.activeUser;
  const roleData = userData.user_roles;

  if (roleData.administrator || roleData.organiser) {
    window.location.href = "/organiser/seminar";
  }
  else{
    if(userData.seminars.length > 1) {
      window.location.href = "/seminars";
    }
    else {
      window.location.href = "/seminars/" + userData.seminars[0].id;
    }
  }
}

export async function completeLogout() {
  await localForage.removeItem("authTokenData");
  window.location.href = "/login";
}
