export const errorMessages = {
  unknownServerError: "Unknown server error",
  formIsNotValid: "Form is not valid",
  pageNotFound: "Page you are looking for is not found",
  formFieldIsRequired: "This field is required"
};
