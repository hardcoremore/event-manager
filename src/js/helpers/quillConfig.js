import Quill from "quill"

const FontAttributor = Quill.import('formats/font');
const fonts = [
  'arial',
  'arial-black',
  'helvetica',
  'sans-serif',
  'verdana',

  'courier',
  'courier-new',
  'monospace',
  'gadget',

  'comic-sans',
  'palatino-linotype',

  'georgia',
  'times-new-roman',
  'serif',

  'impact',
  'charcoal',

  'lucida-console',
  'lucida-sans-unicode'
];

export default function initializeQuill() {
  FontAttributor.whitelist = fonts;
  Quill.register(FontAttributor, true);
}
