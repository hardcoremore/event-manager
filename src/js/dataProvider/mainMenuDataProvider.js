export const organiserMenuItems = [
  {
    label: "Seminars",
    id: "seminar"
  },
  {
    label: "Company Management",
    id: "company"
  },
  {
    label: "User Management",
    id: "user"
  },
  {
    label: "Templates",
    id: "template"
  }
];

export const seminarOrganiserMenuItems = [
  {
    label: "Information",
    id: "information"
  },
  {
    label: "Screen Template",
    id: "template"
  },
  {
    label: "Agenda",
    id: "agenda"
  },
  {
    label: "Participants",
    id: "participants"
  },
  {
    label: "Venue",
    id: "venue"
  },
  {
    label: "Feedback",
    id: "feedback"
  },
  {
    label: "Polls",
    id: "polls"
  },
  {
    label: "Documents",
    id: "documents"
  },
  {
    label: "Preview",
    id: "preview"
  }
];
