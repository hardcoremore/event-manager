import React from 'react';
import { connect } from 'react-redux';
import { navigate } from '@reach/router';
import { getActiveUserData } from '../services/authService';
import actionCreator from '../store/actionCreator';
import seminarActions from '../store/actions/seminar';
import { Button } from 'primereact/button';
import seminarLogo from '../../assets/seminar-logo.png';

class SingleSeminarHomeScreen extends React.Component {
  dashboardButtonClickHandler = event => {
    navigate(event.currentTarget.getAttribute('data-link'));
  };

  componentDidMount() {
    this.props.sendReadSeminarByIdRequest(this.props.recordId);
  }

  render() {
    const userData = getActiveUserData();
    const roleData = userData.user_roles;
    const isAdminOrOrganiser = roleData.administrator || roleData.organiser;
    const displayDasboardButton = isAdminOrOrganiser || userData.seminars.length > 1;

    return (
      <div className="p-grid">
        <div className="p-col-12">
          {displayDasboardButton ? (
            <Button
              className="p-button-info"
              data-link={isAdminOrOrganiser ? '/organiser/seminar' : '/seminars'}
              label="Back to Dashboard"
              onClick={this.dashboardButtonClickHandler}
            />
          ) : null}
        </div>

        {this.props.seminarById ? (
          <div className="p-col-12">
            <img
              src={seminarLogo}
              alt={this.props.seminarById.title}
              title={this.props.seminarById.title}
            />
            <h2>{this.props.seminarById.title}</h2>
          </div>
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = ({ seminar }) => ({
  seminarById: seminar.seminarById
});

const mapDispatchToProps = dispatch => ({
  sendReadSeminarByIdRequest(id) {
    dispatch(actionCreator(seminarActions.sendReadSeminarByIdRequest, id));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SingleSeminarHomeScreen);
