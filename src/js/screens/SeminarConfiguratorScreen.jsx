import React from "react";
import { Router, navigate } from "@reach/router";
import MainMenu from "../components/MainMenu";
import {seminarOrganiserMenuItems} from "../dataProvider/mainMenuDataProvider"
import SeminarOrganiserInformation from "../components/seminar/SeminarOrganiserInformation";
import SeminarOrganiserTemplate from "../components/seminar/SeminarOrganiserTemplate";
import SeminarOrganiserAgenda from "../components/seminar/SeminarOrganiserAgenda";
import SeminarOrganiserParticipants from "../components/seminar/SeminarOrganiserParticipants";

export default class SeminarConfiguratorScreen extends React.Component {

  state = {
    selectedMenuItem: null
  };

  constructor(props) {
    super(props);
    const slugs = window.location.pathname.split("/").slice(-1);
    this.state.selectedMenuItem = slugs[0];
  }

  mainMenuChangeEventHandler = item => {
    navigate(item);
  };

  render() {
    return (
      <div className="p-grid">
        <div className="p-col-2">
          <MainMenu
            model={seminarOrganiserMenuItems}
            selectedItem={this.state.selectedMenuItem}
            onItemChange={this.mainMenuChangeEventHandler}
          />
        </div>
        <Router className="p-col-10">
          <SeminarOrganiserInformation path="/information" />
          <SeminarOrganiserTemplate path="/template" />
          <SeminarOrganiserAgenda path="/agenda" />
          <SeminarOrganiserParticipants path="/participants" />
        </Router>
      </div>
    );
  }
}
