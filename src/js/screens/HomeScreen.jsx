import React from "react";

import eventManagerStore from "../store/eventManagerStore";

export default class HomeScreen extends React.Component {
  componentDidMount() {
    console.log("HOME SCREEN: ", eventManagerStore().getState());
  }
  render() {
    return <div>HOME SCREEN</div>;
  }
}
