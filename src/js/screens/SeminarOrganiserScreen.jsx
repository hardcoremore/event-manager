import React from "react";
import { Router } from "@reach/router";
import SeminarOrganiserHome from "../components/seminar/SeminarOrganiserHome";
import SeminarInformationForm from "../components/seminar/SeminarInformationForm";

export default class SeminarOrganiserScreen extends React.Component {
  render() {
    return (
      <Router>
        <SeminarOrganiserHome path="/" />
        <SeminarInformationForm path="/create" />
      </Router>
    );
  }
}
