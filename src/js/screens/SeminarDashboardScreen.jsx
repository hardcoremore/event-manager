import React from "react";
import { connect } from "react-redux";
import { getActiveUserData } from "../services/authService";
import actionCreator from "../store/actionCreator";
import seminarActions from "../store/actions/seminar";
import AttendSeminarViewItem from "../components/seminar/AttendSeminarViewItem";

class SeminarDashboardScreen extends React.Component {

   componentDidMount() {
    const roleData = getActiveUserData().user_roles;

    if(roleData.administrator || roleData.organiser) {
      window.location.href = "/organiser/seminar";
    }
    else {
      this.props.sendReadAtendSeminarsRequest();
    }
  }

  render() {
    return (
      <div className="p-grid">
        {this.props.attendRecordList.map(seminarData => {
          return (
            <AttendSeminarViewItem
              key={seminarData.id}
              id={seminarData.id}
              title={seminarData.title}
            />
          );
        })}
      </div>
    );
  }
}

const mapStateToProps = ({ seminar }) => ({
  attendRecordList: seminar.attendRecordList
});

const mapDispatchToProps = dispatch => ({
  sendReadAtendSeminarsRequest() {
    dispatch(actionCreator(seminarActions.sendAttendSeminarReadRequest));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SeminarDashboardScreen);

