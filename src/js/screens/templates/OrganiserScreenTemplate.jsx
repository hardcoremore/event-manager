import React from "react";
import { getActiveUserData } from "../../services/authService";
import MainMenu from "../../components/MainMenu";
import { navigate } from "@reach/router";
import { organiserMenuItems } from "../../dataProvider/mainMenuDataProvider";

export default class OrganiserScreenTemplate extends React.Component {
  state = {
    selectedMenuItem: null
  };

  constructor(props) {
    super(props);
    const slugs = window.location.pathname.split("/").slice(1);
    this.state.selectedMenuItem = slugs[1];
  }

  mainMenuChangeEventHandler = item => {
    navigate(this.props.path + "/" + item);
  };

  componentDidMount() {
    const userData = getActiveUserData();
    const roleData = userData.user_roles;

    if(!roleData.administrator && !roleData.organiser) {

        if(userData.seminars.length > 1) {
          window.location.href = "/seminars";
        }
        else {
          window.location.href = "/seminars/" + userData.seminars[0].id;
        }
    }
    else if(!this.state.selectedMenuItem) {
       window.location.href = "/organiser/seminar";
    }
  }

  render() {
    return (
      <div className="p-grid">
        <div className="p-col-2">
          <MainMenu
            model={organiserMenuItems}
            selectedItem={this.state.selectedMenuItem}
            onItemChange={this.mainMenuChangeEventHandler}
          />
        </div>
        <div className="p-col-10">{this.props.children}</div>
      </div>
    );
  }
}
