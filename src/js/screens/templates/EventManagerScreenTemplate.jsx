import React from "react";
import MainHeader from "../../components/MainHeader";
import MainFooter from "../../components/MainFooter";

export default class EventManagerScreenTemplate extends React.Component {
  getMainMenuItems() {
    return;
  }

  render() {
    return (
      <div className="p-grid">
        <div className="p-col-12">
          <MainHeader />
        </div>
        <div className="p-col-12">{this.props.children}</div>
        <div className="p-col-12">
          <MainFooter />
        </div>
      </div>
    );
  }
}
