import React from "react";
import { Button } from "primereact/button";
import { Panel } from "primereact/panel";
import { InputText } from "primereact/inputtext";
import { Password } from "primereact/password";
import actionCreator from "../store/actionCreator";
import authActions from "../store/actions/authentication";
import { connect } from "react-redux";
import { isLoginDataValid } from "../services/authService";
import { navigate } from "@reach/router";
import "../../css/loginScreen.css";

class LoginScreen extends React.Component {
  state = {
    isFormSubmitted: false,
    username: "",
    password: ""
  };

  onUsernameChange = event => {
    this.setState({ username: event.target.value });
  };

  onPasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  submitForm = event => {
    event.preventDefault();
    this.setState({ isFormSubmitted: true });
    this.props.sendLoginRequest(this.state.username, this.state.password);
  };

  async componentDidMount() {
    const isLoginValid = await isLoginDataValid();

    if (isLoginValid) {
      navigate("/");
    }
  }
  static getDerivedStateFromProps(props, state) {
    if (props.loginError) {
      state.isFormSubmitted = false;
    }

    return state;
  }

  render() {
    return (
      <Panel
        header="WELCOME!"
        id="login-screen-container"
        className="title-text-center"
      >
        {this.props.loginError ? (
          <div className="form-error-message">
            {" "}
            {this.props.loginError.message}{" "}
          </div>
        ) : null}

        <form onSubmit={this.submitForm}>
          <div className="p-fluid inputgroup-vertical">
            <label htmlFor="login-input-username">Username</label>
            <InputText
              id="login-input-username"
              name="username"
              onChange={this.onUsernameChange}
            />
          </div>

          <div className="p-fluid inputgroup-vertical">
            <label htmlFor="login-input-password">Password</label>
            <Password
              id="login-input-password"
              name="password"
              onChange={this.onPasswordChange}
            />
          </div>

          <Button
            label="ENTER"
            className="p-button-rounded"
            onClick={this.submitForm}
            disabled={this.state.isFormSubmitted}
          />
        </form>
      </Panel>
    );
  }
}

const mapStateToProps = ({ authentication }) => ({
  loginError: authentication.loginError
});

const mapDispatchToProps = dispatch => ({
  sendLoginRequest(username, password) {
    dispatch(
      actionCreator(authActions.sendLoginRequest, { username, password })
    );
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen);
