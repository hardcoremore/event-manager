import React from "react";
import { Router } from "@reach/router";
import CompanyOrganiserHome from "../components/company/CompanyOrganiserHome";
import CompanyForm from "../components/company/CompanyForm";

export default class CompanyOrganiserScreen extends React.Component {
  render() {
    return (
      <Router>
        <CompanyOrganiserHome path="/" />
        <CompanyForm path="/create" />
        <CompanyForm path="/edit/:recordId" />
      </Router>
    );
  }
}
