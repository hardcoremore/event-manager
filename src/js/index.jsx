import React from 'react';
import { render } from 'react-dom';
import { navigate } from '@reach/router';
import initializeQuill from './helpers/quillConfig';

// this is required to be imported before using async/await in EventManagerApp
// see: https://github.com/babel/babel/issues/5085
import 'babel-polyfill';

import EventManagerApp from './EventManagerApp';
import { Provider } from 'react-redux';
import eventManagerStore, { initializeStore } from './store/eventManagerStore';
import { loadLoginData, isLoginDataValid } from './services/authService';

import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import '../css/main.css';
import '../css/quill-editor.css';
import '../css/theme.css';

async function initializeApp() {
  await loadLoginData();
  const isLoginValid = isLoginDataValid();

  initializeStore();

  initializeQuill();

  if (!isLoginValid) {
    navigate('/login');
  }

  render(
    <div id="event-manager-app-main-container">
      <Provider store={eventManagerStore()}>
        <EventManagerApp />
      </Provider>
    </div>,
    document.getElementById('event-manager-app-main-container')
  );
}

initializeApp();
