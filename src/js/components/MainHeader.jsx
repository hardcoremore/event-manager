import React from 'react';
import { Button } from 'primereact/button';
import { connect } from 'react-redux';
import actionCreator from '../store/actionCreator';
import authActions from '../store/actions/authentication';

class MainHeader extends React.Component {
  logoutButtonClickHandler = () => {
    this.props.sendLogoutRequest();
  };

  render() {
    return (
      <div className="p-grid">
        <div className="p-col-10">
          <Button label="Company Logo" />
        </div>
        <div className="p-col-2">
          <Button label="Logout" onClick={this.logoutButtonClickHandler} />
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  sendLogoutRequest() {
    dispatch(actionCreator(authActions.sendLogoutRequest));
  }
});

const mapStateToProps = ({ authentication }) => ({
  authentication: authentication
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MainHeader);
