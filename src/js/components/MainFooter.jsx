import React from 'react';
import { Button } from 'primereact/button';

export default class MainFooter extends React.Component {
  render() {
    return (
      <div className="p-grid">
        <Button label="Item 1" className="p-col-4" />
        <Button label="Item 2" className="p-col-4" />
        <Button label="Item 3" className="p-col-4" />
      </div>
    );
  }
}
