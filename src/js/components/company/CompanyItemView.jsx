import React from 'react';
import { Button } from 'primereact/button';

export default class CompanyItemView extends React.Component {
  render() {
    return (
      <div>
        <h2>{this.props.name}</h2>
        <h3>Contact email: {this.props.contactEmail}</h3>
        <h3>Created at: {this.props.createdAt}</h3>
        <Button
          label="Edit"
          className="p-button-info"
          onClick={() => this.props.editCompanyButtonClickHandler(this.props.recordId)}
        />
        <Button
          label="X"
          className="p-button-danger"
          onClick={() => this.props.removeCompanyButtonClickHandler(this.props.recordId)}
        />
      </div>
    );
  }
}
