import React from 'react';
import { navigate } from '@reach/router';
import { Button } from 'primereact/button';
import actionCreator from '../../store/actionCreator';
import companyActions from '../../store/actions/company';
import { connect } from 'react-redux';
import CompanyItemView from './CompanyItemView';
import ConfirmModal from '../ConfirmModal';

class CompanyOrganiserHome extends React.Component {
  deleteRecordId = null;

  state = {
    isLoading: false,
    showDeleteModal: false
  };

  componentDidMount() {
    this.props.resetPreviousCompanyData();
    this.props.sendReadRequest();
  }

  createCompanyButtonClickHandler = event => {
    this.props.resetPreviousCompanyData();
    navigate('company/create');
  };

  editCompanyButtonClickHandler = recordId => {
    navigate('company/edit/' + recordId);
  };

  removeCompanyButtonClickHandler = recordId => {
    this.deleteRecordId = recordId;
    this.setState({
      ...this.state,
      showDeleteModal: true
    });
  };

  deleteCompanyConfirmButtonHandler = () => {
    this.setState({
      ...this.state,
      showDeleteModal: false,
      deleteRecordId: null
    });
    this.props.sendDeleteRequest(this.deleteRecordId);
  };

  deleteCompanyCancelButtonHandler = () => {
    this.deleteRecordId = null;
    this.setState({
      ...this.state,
      showDeleteModal: false
    });
  };

  render() {
    return (
      <div className="p-grid">
        <div className="p-col-8">
          <div>
            <div className="p-grid crud-menu-container">
              <div className="p-col-12">
                <Button
                  label="Add Company"
                  className="p-button-success"
                  onClick={this.createCompanyButtonClickHandler}
                />
              </div>
              <div className="p-col-12">
                <div className="p-grid">
                  {this.props.recordList.map(company => {
                    return (
                      <CompanyItemView
                        key={company.id}
                        className="p-col-2"
                        recordId={company.id}
                        name={company.company_name}
                        logo={company.company_logo}
                        contactEmail={company.help_mail_address}
                        createdAt={company.created_at}
                        editCompanyButtonClickHandler={this.editCompanyButtonClickHandler}
                        removeCompanyButtonClickHandler={this.removeCompanyButtonClickHandler}
                      />
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
        </div>

        {this.state.showDeleteModal ? (
          <ConfirmModal
            title="Delete company?"
            confirmButtonHandler={this.deleteCompanyConfirmButtonHandler}
            cancelButtonHandler={this.deleteCompanyCancelButtonHandler}
          >
            <div className="modal-message">
              Are you sure you want to delete this company? This action can not be reversed.
            </div>
          </ConfirmModal>
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = ({ company }) => ({
  recordList: company.recordList,
  readError: company.readError
});

const mapDispatchToProps = dispatch => ({
  sendReadRequest() {
    dispatch(actionCreator(companyActions.sendReadRequest));
  },

  resetPreviousCompanyData() {
    dispatch(actionCreator(companyActions.setLastCreatedCompany, null));
    dispatch(actionCreator(companyActions.setLastUpdatedCompany, null));
  },

  sendDeleteRequest(recordId) {
    dispatch(actionCreator(companyActions.sendDeleteRequest, recordId));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CompanyOrganiserHome);
