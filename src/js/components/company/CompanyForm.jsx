import React from 'react';
import { connect } from 'react-redux';
import { navigate, Redirect } from '@reach/router';
import { Button } from 'primereact/button';
import { Panel } from 'primereact/panel';
import FormTextInputGroup from '../form/FormTextInputGroup';
import actionCreator from '../../store/actionCreator';
import companyActions from '../../store/actions/company';
import ConfirmModal from '../ConfirmModal';

class CompanyForm extends React.Component {
  state = {
    isFormSubmitted: false,
    showCancelModal: false,
    formData: {
      name: '',
      contactEmail: ''
    }
  };

  componentDidMount() {
    if (this.props.recordId) {
      const company = this.props.recordList.find(
        item => item.id.toString() === this.props.recordId.toString()
      );

      if (company) {
        let newState = {
          ...this.state,
          formData: {
            name: company.company_name,
            contactEmail: company.help_mail_address
          }
        };
        this.setState(newState);
      }
    }
  }

  submitForm = event => {
    event.preventDefault();
    this.setState({ isFormSubmitted: true });

    let formData = {
      company_name: this.state.formData.name,
      help_mail_address: this.state.formData.contactEmail,
      company_logo: 'test_logo.png'
    };

    if (this.props.recordId) {
      this.props.sendUpdateRequest(this.props.recordId, formData);
    } else {
      this.props.sendCreateRequest(formData);
    }
  };

  static getDerivedStateFromProps(props, state) {
    if (
      props.createError ||
      props.updateError ||
      props.lastCreatedCompany ||
      props.lastUpdatedCompany
    ) {
      state.isFormSubmitted = false;
    }

    return state;
  }

  onInputChangeHandler = (name, value) => {
    let newState = {
      ...this.state
    };

    newState.formData[name] = value;

    this.setState(newState);
  };

  cancelForm = () => {
    let newState = {
      ...this.state,
      showCancelModal: true
    };

    this.setState(newState);
  };

  cancelFormConfirmButtonHandler = () => {
    this.props.resetPreviousCompanyData();

    navigate('/organiser/company');
  };

  cancelFormCancelButtonHandler = () => {
    this.setState({
      ...this.state,
      showCancelModal: false
    });
  };

  render() {
    const formName = 'company';

    if (!this.props.recordId && this.props.lastCreatedCompany) {
      return <Redirect to="organiser/company" noThrow />;
    }

    const saveError = this.props.createError || this.props.updateError;

    return (
      <Panel
        header={this.props.recordId ? 'Update company' : 'Create new company'}
        id="form-panel-container"
        className="title-text-center"
      >
        {this.props.formError ? (
          <div className="form-error-message"> {this.props.formError.message} </div>
        ) : null}

        <form onSubmit={this.submitForm} className="p-col-12 p-grid">
          <FormTextInputGroup
            prefix={formName}
            name="name"
            label="Company Name"
            value={this.state.formData.name}
            errorMessage={saveError && saveError.errors ? saveError.errors.company_name : null}
            onInputChangeHandler={this.onInputChangeHandler}
          />

          <FormTextInputGroup
            prefix={formName}
            name="contactEmail"
            label="Contact Email"
            value={this.state.formData.contactEmail}
            errorMessage={saveError && saveError.errors ? saveError.errors.help_mail_address : null}
            onInputChangeHandler={this.onInputChangeHandler}
          />
        </form>

        <Button
          name="submit"
          label={this.props.recordId ? 'Update' : 'Create'}
          onClick={this.submitForm}
          disabled={this.state.isFormSubmitted}
        />

        <Button
          label="Cancel"
          onClick={this.cancelForm}
          className="p-button-danger"
          disabled={this.state.isFormSubmitted}
        />

        {this.state.showCancelModal ? (
          <ConfirmModal
            title="Close form?"
            confirmButtonHandler={this.cancelFormConfirmButtonHandler}
            cancelButtonHandler={this.cancelFormCancelButtonHandler}
          >
            <div className="modal-message">
              Are you sure you want close this form? All changes will be lost.
            </div>
          </ConfirmModal>
        ) : null}
      </Panel>
    );
  }
}

const mapStateToProps = ({ company }) => ({
  createError: company.createError,
  updateError: company.updateError,
  recordList: company.recordList,
  lastCreatedCompany: company.lastCreatedCompany,
  lastUpdatedCompany: company.lastUpdatedCompany
});

const mapDispatchToProps = dispatch => ({
  sendCreateRequest(data) {
    dispatch(actionCreator(companyActions.sendCreateRequest, data));
  },

  resetPreviousCompanyData() {
    dispatch(actionCreator(companyActions.setLastCreatedCompany, null));
    dispatch(actionCreator(companyActions.setLastUpdatedCompany, null));
  },

  sendUpdateRequest(recordId, data) {
    dispatch(actionCreator(companyActions.sendUpdateRequest, { recordId, data }));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CompanyForm);
