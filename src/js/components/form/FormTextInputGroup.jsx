import React from 'react';
import { InputText } from 'primereact/inputtext';
import { Message } from 'primereact/message';

class FormTextInputGroup extends React.Component {
  get name() {
    return this.props.name;
  }

  inputChangeEventHandler = event => {
    if (this.props.onInputChangeHandler) {
      this.props.onInputChangeHandler(this.props.name, event.target.value);
    }
  };

  render() {
    const inputId = this.props.prefix + '-' + this.props.name;
    const isVerticalLayout = this.props.layout === 'vertical';

    return (
      <div className="p-col-12 p-grid">
        <label className={isVerticalLayout ? 'p-col-12' : 'p-col-2'} htmlFor={inputId}>
          {this.props.label}
        </label>
        <InputText
          id={inputId}
          className={isVerticalLayout ? 'p-col-12' : 'p-col-3'}
          name={this.props.name}
          value={this.props.value ? this.props.value : ''}
          onChange={this.inputChangeEventHandler}
          disabled={this.props.disabled}
        />
        {this.props.errorMessage ? (
          <div className="p-col-4">
            <Message
              severity="error"
              text={
                Array.isArray(this.props.errorMessage)
                  ? this.props.errorMessage.join(' ')
                  : this.props.errorMessage
              }
            />
          </div>
        ) : (
          <div className="p-col-4" />
        )}
      </div>
    );
  }
}

FormTextInputGroup.defaultProps = {
  layoutType: 'horizontal',
  name: ''
};

export default FormTextInputGroup;
