import React from 'react';
import { Editor } from 'primereact/editor';
import { Message } from 'primereact/message';

const headerTemplate = (
  <div id="toolbar">
    <span className="ql-formats">
      <select className="ql-font">
        <option value="arial">Arial</option>
        <option value="arial-black">Arial Black</option>
        <option value="helvetica">Helvetica</option>
        <option value="sans-serif">Sans</option>
        <option value="verdana">Verdana</option>

        <option value="courier">Courier</option>
        <option value="courier-new">Courier New</option>
        <option value="monospace">Monospace</option>
        <option value="gadget">Gadget</option>

        <option value="comic-sans">Comic Sans MS</option>
        <option value="palatino-linotype">Palatino Linotype</option>

        <option value="georgia">Georgia</option>
        <option value="times-new-roman">Times New Roman</option>
        <option value="serif">Serif</option>

        <option value="impact">Impact</option>

        <option value="lucida-console">Lucida</option>
        <option value="lucida-sans-unicode">Lucida Sans Unicode</option>
      </select>

      <select className="ql-size">
        <option defaultValue />
        <option value="small" />
        <option value="large" />
        <option value="huge" />
      </select>
      <select className="ql-color" />
      <select className="ql-background" />
    </span>

    <span className="ql-formats">
      <button className="ql-bold" aria-label="Bold" />
      <button className="ql-italic" aria-label="Italic" />
      <button className="ql-underline" aria-label="Underline" />
      <button className="ql-strike" />
      <button className="ql-blockquote" aria-label="Blockquote" />
      <button className="ql-script" value="sub" />
      <button className="ql-script" value="super" />
    </span>
    <span className="ql-formats">
      <button className="ql-list" value="ordered" />
      <button className="ql-list" value="bullet" />
      <button className="ql-link" />
    </span>
  </div>
);

class FormTextEditorInputGroup extends React.Component {
  get name() {
    return this.props.name;
  }

  editorTextChange = event => {
    if (this.props.onInputChangeHandler) {
      this.props.onInputChangeHandler(this.props.name, event.htmlValue);
    }
  };

  render() {
    const inputId = this.props.prefix + '-' + this.props.name;
    const isVerticalLayout = this.props.layout === 'vertical';

    return (
      <div className="p-col-12 p-grid p-fluid">
        <label className={isVerticalLayout ? 'p-col-12' : 'p-col-2'} htmlFor={inputId}>
          {this.props.label}
        </label>

        <Editor
          id={inputId}
          name={this.props.name}
          value={this.props.value ? this.props.value : ''}
          className={isVerticalLayout ? 'p-col-12' : 'p-col-6'}
          style={{ height: '260px' }}
          onTextChange={this.editorTextChange}
          headerTemplate={headerTemplate}
          readonly={this.props.disabled}
        />

        {this.props.errorMessage ? (
          <div className="p-col-4">
            <Message
              severity="error"
              text={
                Array.isArray(this.props.errorMessage)
                  ? this.props.errorMessage.join(' ')
                  : this.props.errorMessage
              }
            />
          </div>
        ) : (
          <div className="p-col-4" />
        )}
      </div>
    );
  }
}

FormTextEditorInputGroup.defaultProps = {
  layoutType: 'horizontal',
  name: ''
};

export default FormTextEditorInputGroup;
