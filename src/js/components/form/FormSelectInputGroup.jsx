import React from 'react';
import { Dropdown } from 'primereact/dropdown';
import { Message } from 'primereact/message';

class FormSelectInputGroup extends React.Component {
  state = {
    initialSelectedValue: null,
    selectedValue: null
  };

  get name() {
    return this.props.name;
  }

  selectChangeEventHandler = event => {
    this.setState({
      ...this.state,
      selectedValue: event.value
    });

    if (this.props.onInputChangeHandler) {
      this.props.onInputChangeHandler(this.props.name, event.value);
    }
  };

  static getDerivedStateFromProps(props, state) {
    if (props.selectedValue && props.selectedValue !== state.initialSelectedValue) {
      state.selectedValue = props.selectedValue;
      state.initialSelectedValue = props.selectedValue;
    }

    return state;
  }

  render() {
    const inputId = this.props.prefix + '-' + this.props.name;
    const isVerticalLayout = this.props.layout === 'vertical';

    return (
      <div className="p-col-12 p-grid">
        <label className={isVerticalLayout ? 'p-col-12' : 'p-col-2'} htmlFor={inputId}>
          {this.props.label}
        </label>
        <Dropdown
          inputId={inputId}
          autoWidth={false}
          className={isVerticalLayout ? 'p-col-12' : 'p-col-3'}
          name={this.props.name}
          options={this.props.options}
          value={this.state.selectedValue}
          placeholder={this.props.placeHolder}
          optionLabel={this.props.labelProperty}
          valueKey={this.props.valueProperty}
          onChange={this.selectChangeEventHandler}
          disabled={this.props.disabled}
        />
        {this.props.errorMessage ? (
          <div className="p-col-4">
            <Message
              severity="error"
              text={
                Array.isArray(this.props.errorMessage)
                  ? this.props.errorMessage.join(' ')
                  : this.props.errorMessage
              }
            />
          </div>
        ) : (
          <div className="p-col-4" />
        )}
      </div>
    );
  }
}

FormSelectInputGroup.defaultProps = {
  layoutType: 'horizontal',
  name: '',
  options: [],
  selectedValue: null,
  labelProperty: 'label',
  valueProperty: 'value',
  disabled: false
};

export default FormSelectInputGroup;
