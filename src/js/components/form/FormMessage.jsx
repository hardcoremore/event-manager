import React from 'react';

class FormMessage extends React.Component {
  render() {
    return (
      <div className={`${this.props.className} form-message-container ${this.props.type}`}>
        {this.props.message}
      </div>
    );
  }
}

FormMessage.defaultProps = {
  type: 'error'
};

export default FormMessage;
