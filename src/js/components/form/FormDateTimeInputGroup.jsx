import React from 'react';
import { Calendar } from 'primereact/calendar';
import { Message } from 'primereact/message';

class FormDateTimeInputGroup extends React.Component {
  get name() {
    return this.props.name;
  }

  inputChangeEventHandler = event => {
    if (this.props.onInputChangeHandler) {
      this.props.onInputChangeHandler(this.props.name, event.target.value);
    }
  };

  render() {
    const inputId = this.props.prefix + '-' + this.props.name;
    const isVerticalLayout = this.props.layout === 'vertical';

    return (
      <div className="p-col-12 p-grid p-fluid">
        <label className={isVerticalLayout ? 'p-col-12' : 'p-col-2'} htmlFor={inputId}>
          {this.props.label}
        </label>
        <div className={isVerticalLayout ? 'p-col-12' : 'p-col-3'}>
          <Calendar
            id={inputId}
            name={this.props.name}
            readOnlyInput={true}
            value={this.props.value ? this.props.value : ''}
            onChange={this.inputChangeEventHandler}
            showTime={this.props.showTime}
            dateFormat={this.props.dateFormat}
            hourFormat={this.props.hourFormat}
            disabled={this.props.disabled}
            showIcon={true}
            selectOtherMonths={true}
          />
        </div>

        {this.props.errorMessage ? (
          <div className="p-col-4">
            <Message
              severity="error"
              text={
                Array.isArray(this.props.errorMessage)
                  ? this.props.errorMessage.join(' ')
                  : this.props.errorMessage
              }
            />
          </div>
        ) : (
          <div className="p-col-4" />
        )}
      </div>
    );
  }
}

FormDateTimeInputGroup.defaultProps = {
  layoutType: 'horizontal',
  name: ''
};

export default FormDateTimeInputGroup;
