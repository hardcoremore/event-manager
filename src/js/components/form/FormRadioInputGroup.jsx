import React from 'react';
import { RadioButton } from 'primereact/radiobutton';
import { Message } from 'primereact/message';

class FormRadioInputGroup extends React.Component {
  state = {
    selectedValue: null
  };

  constructor(props) {
    super(props);
    this.state.selectedValue = props.selectedValue;
  }

  get name() {
    return this.props.name;
  }

  radioChangeEventHandler = event => {
    const selectedValue = event.target.value;
    this.setState({
      selectedValue: selectedValue
    });

    if (this.props.onInputChangeHandler) {
      this.props.onInputChangeHandler(this.props.name, selectedValue);
    }
  };

  render() {
    const inputId = this.props.prefix + '-' + this.props.name;
    const isVerticalLayout = this.props.layout === 'vertical';

    return (
      <div className="p-col-12 p-grid">
        <label className={isVerticalLayout ? 'p-col-12' : 'p-col-2'} htmlFor={inputId}>
          {this.props.label}
        </label>
        <div className={isVerticalLayout ? 'p-col-12' : 'p-col-3'}>
          {this.props.options.map(option => {
            let optionId = inputId + '-' + option.value;
            return (
              <div key={option.value}>
                <RadioButton
                  key={option.value}
                  inputId={optionId}
                  name={this.props.name}
                  value={option.value}
                  checked={this.state.selectedValue === option.value}
                  onChange={this.radioChangeEventHandler}
                  disabled={this.props.disabled}
                />
                <label htmlFor={optionId} className="p-radiobutton-label">
                  {option.label}
                </label>
              </div>
            );
          })}
        </div>
        {this.props.errorMessage ? (
          <div className="p-col-4">
            <Message
              severity="error"
              text={
                Array.isArray(this.props.errorMessage)
                  ? this.props.errorMessage.join(' ')
                  : this.props.errorMessage
              }
            />
          </div>
        ) : (
          <div className="p-col-4" />
        )}
      </div>
    );
  }
}

FormRadioInputGroup.defaultProps = {
  layoutType: 'horizontal',
  name: '',
  options: [],
  disabled: false
};

export default FormRadioInputGroup;
