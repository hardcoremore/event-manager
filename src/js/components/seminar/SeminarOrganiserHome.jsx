import React from 'react';
import { connect } from 'react-redux';
import { navigate, Link } from '@reach/router';
import { Button } from 'primereact/button';
import actionCreator from '../../store/actionCreator';
import seminarActions from '../../store/actions/seminar';
import FormSelectInputGroup from '../form/FormSelectInputGroup';
import AttendSeminarViewItem from './AttendSeminarViewItem';
import EditSeminarContainerItem from './EditSeminarContainerItem';

import ConfirmModal from '../ConfirmModal';

class SeminarOrganiserHome extends React.Component {
  deleteRecordId = null;
  selectedSeminar = null;

  state = {
    isLoading: false,
    showDeleteModal: false,
    showDuplicateSeminarModal: false
  };

  componentDidMount() {
    this.props.resetPreviousSeminarData();
    this.props.sendReadAtendSeminarsRequest();
    this.props.sendReadEditSeminarsRequest();
  }

  editSeminarButtonClickHandler = recordId => {
    navigate('/organiser/seminar/edit/' + recordId + '/information');
  };

  removeSeminarButtonClickHandler = recordId => {
    this.deleteRecordId = recordId;
    this.setState({
      ...this.state,
      showDeleteModal: true
    });
  };

  deleteSeminarConfirmButtonHandler = () => {
    this.setState({
      ...this.state,
      showDeleteModal: false,
      deleteRecordId: null
    });
    this.props.sendDeleteRequest(this.deleteRecordId);
  };

  deleteSeminarCancelButtonHandler = () => {
    this.deleteRecordId = null;
    this.setState({
      ...this.state,
      showDeleteModal: false
    });
  };

  duplicateSeminarSelectChangeHandler = event => {
    this.selectedSeminar = event.target.value;
  };

  duplicateSeminarButtonClickHandler = () => {
    this.setState({
      ...this.state,
      showDuplicateSeminarModal: true
    });
  };

  duplicateSeminarConfirmButtonHandler = () => {
    this.setState({
      ...this.state,
      showDuplicateSeminarModal: false
    });
  };

  duplicateSeminarCancelButtonHandler = () => {
    this.setState({
      ...this.state,
      showDuplicateSeminarModal: false
    });
  };

  render() {
    return (
      <div className="p-grid">
        <div className="p-col-8">
          <div>
            <div className="p-grid">
              <div className="screen-title p-col-8">Seminars</div>

              <div className="crud-menu-container p-col-4 p-grid">
                <Link to="create" className="p-col-6">
                  <Button label="New Seminar" className="p-button-success" />
                </Link>

                <div className="p-col-6">
                  <Button
                    label="Duplicate Seminar"
                    className="p-button-info"
                    onClick={this.duplicateSeminarButtonClickHandler}
                  />
                </div>
              </div>

              {this.props.attendRecordList.length > 0 ? (
                <div className="p-grid p-col-12">
                  {this.props.attendRecordList.map(seminarData => {
                    return (
                      <AttendSeminarViewItem
                        key={seminarData.id}
                        id={seminarData.id}
                        title={seminarData.title}
                      />
                    );
                  })}
                </div>
              ) : null}
              {this.props.editRecordList.map(seminarData => {
                return (
                  <EditSeminarContainerItem
                    key={seminarData.id}
                    className="p-col-12"
                    companyName={seminarData.company_name}
                    seminars={seminarData.seminars}
                    editSeminarButtonClickHandler={this.editSeminarButtonClickHandler}
                    removeSeminarButtonClickHandler={this.removeSeminarButtonClickHandler}
                  />
                );
              })}
            </div>
          </div>
        </div>

        {this.state.showDeleteModal ? (
          <ConfirmModal
            title="Delete Seminar?"
            confirmButtonHandler={this.deleteSeminarConfirmButtonHandler}
            cancelButtonHandler={this.deleteSeminarCancelButtonHandler}
          >
            <div className="modal-message">
              Are you sure you want to delete this seminar? This action can not be reversed.
            </div>
          </ConfirmModal>
        ) : null}

        {this.state.showDuplicateSeminarModal ? (
          <ConfirmModal
            title="Duplicate Seminar?"
            confirmButtonHandler={this.duplicateSeminarConfirmButtonHandler}
            cancelButtonHandler={this.duplicateSeminarCancelButtonHandler}
          >
            <div className="modal-message">
              Please select seminar you want to duplicate:
              <FormSelectInputGroup
                prefix="duplicate-seminar-list"
                options={this.props.editRecordList.seminars}
                valueProperty="id"
                labelProperty="title"
                placeHolder="Select seminar"
                onInputChangeHandler={this.duplicateSeminarSelectChangeHandler}
              />
            </div>
          </ConfirmModal>
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = ({ seminar }) => ({
  attendRecordList: seminar.attendRecordList,
  editRecordList: seminar.editRecordList,
  readError: seminar.readError
});

const mapDispatchToProps = dispatch => ({
  sendReadAtendSeminarsRequest() {
    dispatch(actionCreator(seminarActions.sendAttendSeminarReadRequest));
  },

  sendReadEditSeminarsRequest() {
    dispatch(actionCreator(seminarActions.sendEditSeminarReadRequest));
  },

  resetPreviousSeminarData() {
    dispatch(actionCreator(seminarActions.setLastCreatedSeminar, null));
  },

  sendDeleteRequest(recordId) {
    dispatch(actionCreator(seminarActions.sendDeleteRequest, recordId));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SeminarOrganiserHome);
