import React from 'react';
import EditSeminarViewItem from './EditSeminarViewItem';

export default class EditSeminarContainerItem extends React.Component {
  render() {
    return (
      <div>
        <h1>{this.props.companyName}</h1>
        <div>
          {this.props.seminars.map(singleSeminar => {
            return (
              <EditSeminarViewItem
                key={singleSeminar.id}
                recordId={singleSeminar.id}
                title={singleSeminar.title}
                description={singleSeminar.event_description}
                startDate={singleSeminar.start_date}
                endDate={singleSeminar.end_date}
                publishedFrom={singleSeminar.published_from}
                publishedTo={singleSeminar.published_to}
                timeZone={singleSeminar.time_zone}
                onSeminarEditButtonClickHandler={this.props.editSeminarButtonClickHandler}
                onSeminarRemoveButtonClickHandler={this.props.removeSeminarButtonClickHandler}
              />
            );
          })}
        </div>
      </div>
    );
  }
}
