import React from 'react';
import { connect } from 'react-redux';
import { navigate, Redirect } from '@reach/router';
import { getActiveUserData } from '../../services/authService';
import companyActions from '../../store/actions/company';
import seminarActions from '../../store/actions/seminar';
import actionCreator from '../../store/actionCreator';
import { Message } from 'primereact/message';
import FormMessage from '../form/FormMessage';
import FormTextInputGroup from '../form/FormTextInputGroup';
import FormSelectInputGroup from '../form/FormSelectInputGroup';
import FormTextEditorInputGroup from '../form/FormTextEditorInputGroup';
import FormDateTimeInputGroup from '../form/FormDateTimeInputGroup';
import FormRadioInputGroup from '../form/FormRadioInputGroup';
import timeZoneList from '../../dataProvider/timeZoneList';
import { errorMessages } from '../../helpers/errorMessages';
import moment from 'moment';
import { displayDateFormat, serverDateFormat } from '../../helpers/date';

import { Button } from 'primereact/button';

const formPrefix = 'newSeminarForm';

const timeZoneFormatList = [
  {
    label: '12h',
    value: '12h'
  },
  {
    label: '24h',
    value: '24h'
  }
];

class SeminarInformationForm extends React.Component {
  state = {
    isFormSubmitted: false,
    isEditMode: false,
    formData: {
      title: null,
      company: null,
      description: null,
      startDate: null,
      endDate: null,
      publishedFrom: null,
      publishedTo: null,
      timeZone: null,
      timeZoneFormat: null
    }
  };

  static getDerivedStateFromProps(props, state) {
    // wait for companies to load
    if (props.companies.length === 0) {
      return state;
    }

    if (props.createRequestError || props.updateRequestError) {
      state.isFormSubmitted = false;
    }

    let newState = state;

    if (props.recordId && !state.isEditMode) {
      let seminarItem;

      newState = {
        ...state,
        isEditMode: true
      };

      props.editRecordList.find(item => {
        const seminarData = item.seminars.find(
          seminar => seminar.id.toString() === props.recordId.toString()
        );

        if (seminarData) {
          seminarItem = seminarData;
        }
        return seminarData !== undefined;
      });

      if (seminarItem) {
        newState.formData = {
          title: seminarItem.title,
          company: props.companies.find(
            company => company.id.toString() === seminarItem.company_id.toString()
          ),
          description: seminarItem.event_description,
          startDate: moment(seminarItem.start_date).toDate(),
          endDate: moment(seminarItem.end_date).toDate(),
          publishedFrom: moment(seminarItem.published_from).toDate(),
          publishedTo: moment(seminarItem.published_to).toDate(),
          timeZone: timeZoneList.find(item => item.value === seminarItem.time_zone),
          timeFormat: seminarItem.time_format
        };
      }
    }

    if (
      props.createError ||
      props.updateError ||
      props.lastCreatedSeminar ||
      props.lastUpdatedSeminar
    ) {
      newState.isFormSubmitted = false;
    }

    return newState;
  }

  onInputChangeHandler = (name, value) => {
    const formData = {
      ...this.state.formData,
      [name]: value
    };

    this.setState({
      ...this.state,
      formData
    });
  };

  cancelFormButtonClickHandler = () => {
    this.props.resetPreviousSeminarData();
    navigate('/organiser/seminar');
  };

  submitFormButtonClickHandler = event => {
    event.preventDefault();

    if (!this.validateForm()) {
      return;
    }

    const formData = {
      title: this.state.formData.title,
      event_description: this.state.formData.description,
      start_date: moment(this.state.formData.startDate).format(serverDateFormat),
      end_date: moment(this.state.formData.endDate).format(serverDateFormat),
      published_from: moment(this.state.formData.publishedFrom).format(serverDateFormat),
      published_to: moment(this.state.formData.publishedTo).format(serverDateFormat),
      time_zone: this.state.formData.timeZone.value,
      time_format: this.state.formData.timeFormat
    };

    if (this.props.recordId) {
      formData.company = this.state.formData.company.id;

      this.props.sendUpdateRequest(this.props.recordId, formData);
    } else {
      this.props.sendCreateRequest(this.getCompanyId(), formData);
    }

    this.setState({ ...this.state, isFormSubmitted: true });
  };

  getCompanyId() {
    if (getActiveUserData().user_roles.administrator) {
      if (this.state.formData.company) {
        return this.state.formData.company.id;
      } else {
        return null;
      }
    } else {
      return getActiveUserData().companies[0].id;
    }
  }

  validateForm() {
    const isCompanyValid = this.getCompanyId() !== null;
    const isTimeZoneValid = this.state.formData.timeZone !== null;

    let formError;

    if (!isCompanyValid || !isTimeZoneValid) {
      formError = {
        code: 422,
        message: errorMessages.formIsNotValid,
        errors: {}
      };

      if (!isCompanyValid) {
        formError.errors.company = errorMessages.formFieldIsRequired;
      }

      if (!isTimeZoneValid) {
        formError.errors.time_zone = errorMessages.formFieldIsRequired;
      }

      if (this.props.recordId) {
        this.props.setUpdateFormValidationError(formError);
      } else {
        this.props.setCreateFormValidationError(formError);
      }

      return false;
    }

    return true;
  }

  componentDidMount() {
    this.props.readAllCompanies();
  }

  render() {
    if (!this.props.recordId && this.props.lastCreatedSeminar) {
      return (
        <Redirect
          to={`/organiser/seminar/edit/${this.props.lastCreatedSeminar.id}/information`}
          noThrow
        />
      );
    }

    const saveError = this.props.createError || this.props.updateError;

    return (
      <div className="p-grid">
        <div className="p-grid p-col-12">
          <h1 className="p-col-8">
            {this.props.recordId ? 'Seminar Information' : 'Create New Seminar'}
          </h1>
          <div className="p-col-4">
            <Button
              label="Cancel"
              className="p-button-danger"
              disabled={this.state.isFormSubmitted}
              onClick={this.cancelFormButtonClickHandler}
            />
            <Button
              label={this.props.recordId ? 'Save seminar' : 'Create new Seminar'}
              className="p-button-success"
              disabled={this.state.isFormSubmitted}
              onClick={this.submitFormButtonClickHandler}
            />
          </div>
        </div>

        {saveError && saveError.message ? (
          <div className="p-grid p-col-12">
            <FormMessage className="p-col-8" message={saveError.message} />
            <div className="p-col-4" />
          </div>
        ) : null}

        <div className="p-col-12">
          <FormTextInputGroup
            prefix={formPrefix}
            name="title"
            label="Seminar Name"
            value={this.state.formData.title}
            errorMessage={saveError && saveError.errors ? saveError.errors.title : null}
            onInputChangeHandler={this.onInputChangeHandler}
          />

          {getActiveUserData().user_roles.administrator ? (
            <FormSelectInputGroup
              name="company"
              prefix={formPrefix}
              options={this.props.companies}
              selectedValue={this.state.formData.company}
              valueProperty="id"
              labelProperty="company_name"
              label="Select company"
              placeHolder="Select Company"
              errorMessage={saveError && saveError.errors ? saveError.errors.company : null}
              onInputChangeHandler={this.onInputChangeHandler}
            />
          ) : null}

          <FormTextEditorInputGroup
            prefix={formPrefix}
            name="description"
            label="Seminar Description"
            value={this.state.formData.description}
            errorMessage={saveError && saveError.errors ? saveError.errors.event_description : null}
            onInputChangeHandler={this.onInputChangeHandler}
          />

          <FormDateTimeInputGroup
            prefix={formPrefix}
            name="publishedFrom"
            label="Published From"
            value={this.state.formData.publishedFrom}
            errorMessage={saveError && saveError.errors ? saveError.errors.published_from : null}
            onInputChangeHandler={this.onInputChangeHandler}
            showTime={true}
            hourFormat="24"
            dateFormat={displayDateFormat}
          />

          <FormDateTimeInputGroup
            prefix={formPrefix}
            name="publishedTo"
            label="Published Until"
            value={this.state.formData.publishedTo}
            errorMessage={saveError && saveError.errors ? saveError.errors.published_to : null}
            onInputChangeHandler={this.onInputChangeHandler}
            showTime={true}
            hourFormat="24"
            dateFormat={displayDateFormat}
          />

          <FormDateTimeInputGroup
            prefix={formPrefix}
            name="startDate"
            label="Seminar Start"
            value={this.state.formData.startDate}
            errorMessage={saveError && saveError.errors ? saveError.errors.start_date : null}
            onInputChangeHandler={this.onInputChangeHandler}
            showTime={true}
            hourFormat="24"
            dateFormat={displayDateFormat}
          />

          <FormDateTimeInputGroup
            prefix={formPrefix}
            name="endDate"
            label="Seminar End"
            value={this.state.formData.endDate}
            errorMessage={saveError && saveError.errors ? saveError.errors.end_date : null}
            onInputChangeHandler={this.onInputChangeHandler}
            showTime={true}
            hourFormat="24"
            dateFormat={displayDateFormat}
          />

          <FormSelectInputGroup
            name="timeZone"
            prefix={formPrefix}
            options={timeZoneList}
            selectedValue={this.state.formData.timeZone}
            valueProperty="value"
            labelProperty="text"
            placeHolder="Time Zone"
            errorMessage={saveError && saveError.errors ? saveError.errors.time_zone : null}
            onInputChangeHandler={this.onInputChangeHandler}
          />

          <FormRadioInputGroup
            name="timeFormat"
            prefix={formPrefix}
            options={timeZoneFormatList}
            selectedValue={this.state.formData.timeFormat}
            labelProperty="label"
            valueProperty="value"
            label="Time Zone"
            errorMessage={saveError && saveError.errors ? saveError.errors.time_format : null}
            onInputChangeHandler={this.onInputChangeHandler}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ seminar, company }) => ({
  editRecordList: seminar.editRecordList,
  createError: seminar.createError,
  updateError: seminar.updateError,
  companies: company.recordList,
  lastCreatedSeminar: seminar.lastCreatedSeminar,
  lastUpdatedSeminar: seminar.lastUpdatedSeminar
});

const mapDispatchToProps = dispatch => ({
  readAllCompanies() {
    dispatch(actionCreator(companyActions.sendReadRequest));
  },

  sendCreateRequest(companyId, data) {
    dispatch(actionCreator(seminarActions.sendCreateRequest, { companyId, data }));
  },

  resetPreviousSeminarData() {
    dispatch(actionCreator(seminarActions.setLastCreatedSeminar, null));
    dispatch(actionCreator(seminarActions.setLastUpdatedSeminar, null));
  },

  setCreateFormValidationError(error) {
    dispatch(actionCreator(seminarActions.createRequestError, error));
  },

  setUpdateFormValidationError(error) {
    dispatch(actionCreator(seminarActions.updateRequestError, error));
  },

  sendUpdateRequest(recordId, data) {
    dispatch(actionCreator(seminarActions.sendUpdateRequest, { recordId, data }));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SeminarInformationForm);
