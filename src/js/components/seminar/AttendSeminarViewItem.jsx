import React from 'react';
import { Link } from '@reach/router';
import seminarLogo from '../../../assets/seminar-logo.png';

export default class AttendSeminarViewItem extends React.Component {
  render() {
    return (
      <div>
        <Link to={`/seminars/${this.props.id}`}>
          <img src={seminarLogo} alt={this.props.title} title={this.props.title} />
          <h2>{this.props.title}</h2>
        </Link>
      </div>
    );
  }
}
