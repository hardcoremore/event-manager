import React from 'react';
import { Button } from 'primereact/button';
import sanitizeHtml from 'sanitize-html';
import seminarLogo from '../../../assets/seminar-logo.png';
import moment from 'moment';

const seminarMomentDateFormat = 'DD.MM.YYYY';

export default class EditSeminarViewItem extends React.Component {
  render() {
    return (
      <div className="edit-seminar-container p-grid">
        <div className="p-grid p-col-4 seminar-basic-info-container">
          <img
            className="p-col-12"
            src={seminarLogo}
            alt={this.props.title}
            title={this.props.title}
          />
          <div className="seminar-title p-col-12">{sanitizeHtml(this.props.title)}</div>
        </div>

        <div className="p-grid p-col-8">
          <div className="p-grid edit-seminar-info">
            <div className="p-grid p-col-10">
              <div className="p-col-2">Seminar: </div>
              <div className="p-col-10">
                {moment(this.props.startDate).format(seminarMomentDateFormat)} -{' '}
                {moment(this.props.endDate).format(seminarMomentDateFormat)}
              </div>

              <div className="p-col-2">Published: </div>
              <div className="p-col-10">
                {moment(this.props.publishedFrom).format(seminarMomentDateFormat)} -{' '}
                {moment(this.props.publishedTo).format(seminarMomentDateFormat)}
              </div>
            </div>

            <div className="p-col-2">
              <Button
                label="Edit"
                className="p-button-info"
                onClick={() => this.props.onSeminarEditButtonClickHandler(this.props.recordId)}
              />
              <Button
                label="Delete"
                className="p-button-danger"
                onClick={() => this.props.onSeminarRemoveButtonClickHandler(this.props.recordId)}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
