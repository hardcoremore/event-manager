import React from 'react';
import { Button } from 'primereact/button';

export default class MainMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItem: props.selectedItem,
      items: props.model
    };
  }

  getClassNameForMenuItem(id) {
    let className = 'p-col-12 ';
    this.state.selectedItem === id
      ? (className += 'p-button-info menu-item-active')
      : (className += 'p-button-secondary');

    return className;
  }

  menuItemClickHandler = event => {
    const itemId = event.currentTarget.getAttribute('data-id');

    if (this.props.onItemChange && !event.currentTarget.classList.contains('menu-item-active')) {
      this.props.onItemChange(itemId);
    }

    this.setState({ selectedItem: itemId });
  };

  render() {
    return (
      <div className="main-menu">
        {this.state.items.map(item => {
          return (
            <Button
              key={item.id}
              data-id={item.id}
              label={item.label}
              className={this.getClassNameForMenuItem(item.id)}
              onClick={this.menuItemClickHandler}
            />
          );
        })}
      </div>
    );
  }
}
