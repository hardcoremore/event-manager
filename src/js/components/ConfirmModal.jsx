import React from 'react';
import { createPortal } from 'react-dom';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';

const modalRoot = document.getElementById('modal-container');

export default class ConfirmModal extends React.Component {
  constructor(props) {
    super(props);

    this.el = document.createElement('div');
  }

  componentDidMount() {
    modalRoot.appendChild(this.el);
  }

  componentWillUnmount() {
    modalRoot.removeChild(this.el);
  }

  render() {
    return createPortal(
      <div className="modal-container">
        <Card title={this.props.title}>
          {this.props.children}

          <div className="p-grid">
            <Button
              label="Yes"
              className="p-button-danger p-col-3"
              onClick={this.props.confirmButtonHandler}
            />
            <div className="p-col-6" />
            <Button
              label="No"
              className="p-button-info p-col-3"
              onClick={this.props.cancelButtonHandler}
            />
          </div>
        </Card>
      </div>,
      this.el
    );
  }
}
